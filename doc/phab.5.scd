phab(5)

# NAME

phab - configuration file for *phab*)(1)

# DESCRIPTION

A phab configuration file is a simple ini-style file.
parameters that may need to be changed in future. This includes API secrets,
for better or worse. As sensitive credentials are stored, the file should not
be granted world-readable access (phab will refuse to operate, too).

phab searches for a config file in the following locations, in this order:

. $PWD/phab.cfg
. $XDG_CONFIG_HOME/phab/phab.cfg
. /etc/phab/phab.cfg

If unset, $XDG_CONFIG_HOME defaults to *~/.config*.

There are no defaults; Each option must be supplied in the configuration.

# OPTIONS

## [main]

Block mapping containing credentials for the MarkMonitor API.

	*default*
		The default Phabricator configuration to use when issuing commands.

	*username*
		MarkMonitor API username

	*password*
		MarkMonitor API password

## [_configuration name_]

Multiple instance configurations can be set in the configuration file. The
default can then be set in *[main]'*s *default* option.

	*key*
		The Phabricator API key to use.

	*default_project*
		The project to use when unspecified by the user.

	*url*
		The Phabricator endpoint to use.

	*username*
		The Phabricator API username to use

## Example configuration

	```
	[main]
	default = prod

	[prod]
	default_project = myteam
	key = api-1234
	url = https://phabricator.example.com
	username = myuser

# AUTHORS

Maintained by Wikimedia Foundation SRE team <sre@wikimedia.org>. For more
information about development, see <https://gitlab.wikimedia.org/repos/sre/phab>.

# SEE ALSO

*phab*(1)
