# phab

Phabricator command line client. This builds upon the library from
[phabricator-maintenance-bot](https://gitlab.wikimedia.org/ladsgroup/Phabricator-maintenance-bot).

This is still experimental

## Quick Setup

```
$ mkdir ~/.phab
$ cat <<EOL >> ~/.phab/phab.cfg
[main]
default = wikimedia

[wikimedia]
cache_path = ~/.phab
key = api-myapi
default_project = serviceops
url = https://phabricator.wikimedia.org
username = this_beautiful_user
EOL

$ cd /path/to/source
$ git submodule init
$ git submodule update
$ pip install .

```

### Commands
```
$ phab-cli ls

$ phab-cli ls Incoming --limit 30

$ phab-cli view T377311

$ phab-cli mv T377311 Doing

$ phab-cli comments T377311 --limit 2

$ phab-cli setprio T377311 normal

$ phab-cli setstatus T377311 open

$ phab-cli rmtag T377311 SRE
```
## Configuration and usage

View phab(1) for program usage. View phab(5) and the example doc/phab.cfg file
for information on configuration.
