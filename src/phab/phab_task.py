#!/usr/bin/env python

"""
SPDX-License-Identifier: GPL-3.0-or-later

TODO: Add lastModified in index
"""
from datetime import datetime
import logging
import time
try:
    from phab_transaction import Transaction
except ImportError:
    from .phab_transaction import Transaction

logger = logging.getLogger(__name__)

def makeTheDate(date):
    _date = {'timestamp': int(date),
             'human': f"{datetime.fromtimestamp(int(date)).strftime('%Y-%m-%d %H:%M')} UTC"
             }
    return _date


def timeIt(func):
    def wrapit(*args, **kwargs):
        start_time = time.perf_counter()
        result = func(*args, **kwargs)
        end_time = time.perf_counter()
        logger.debug("%s executed in %s ms",
                     func.__name__, (end_time - start_time)*1000)
        return result
    return wrapit


class Task():
    '''
    We do not always need all the information about a task, we may only need to find the task_phid, and then
    use that for other operations, ie we will need some lazy loading here.

    This class to represent a Phabricator task. Task data are retrieved from the api, and they are basically
    a dictionary with the following structure (in json as stored in the cache):
    "id": 346971,
    "type": "TASK",
    "phid": "PHID-TASK-fopml",
    "fields": {
        "name": "Title",
        "description": {
          "raw": "Descr"
        },
        "authorPHID": "PHID-USER-6diqh4",
        "ownerPHID": null,
        "status": {
          "value": "open",
          "name": "Open",
          "color": null
        },
        "priority": {
          "value": 25,
          "name": "Low",
          "color": "yellow"
        },
        "points": null,
        "subtype": "error",
        "closerPHID": null,
        "dateClosed": null,
        "spacePHID": "PHID-SPCE-6l6g5p53",
        "dateCreated": 1695234978,
        "dateModified": 1718295274,
        "policy": {
          "view": "public",
          "interact": "public",
          "edit": "users"
        },
        "custom.deadline.due": null,
        "custom.train.status": null,
        "custom.train.backup": null,
        "custom.external_reference": null,
        "custom.release.version": null,
        "custom.release.date": null,
        "custom.security_topic": "default",
        "custom.risk.summary": null,
        "custom.risk.impacted": null,
        "custom.risk.rating": null,
        "custom.requestor.affiliation": null,
        "custom.error.reqid": null,
        "custom.error.stack": null,
        "custom.error.url": "https://commons.wikimedia.org/wiki/Special:EntityData/M137871102.ttl?flavor=*&revision=*",
        "custom.error.id": null,
        "custom.points.final": null,
        "custom.deadline.start": null
      },
      "attachments": {
        "projects": {
          "projectPHIDs": [
            "PHID-PROJ-3sro5uw",
          ]

    '''
    def __init__(self, task_info, PhabClientLCache, url='https://phabricator.wikimedia.org',
                 cache=None, refresh=False, transactions=[]):
        try:
            self.task_phid = task_info['phid']
            self.number = f"T{task_info['id']}"
            self.title = task_info['fields']['name']
            self.description = task_info['fields']['description']['raw']
            self.priority = task_info['fields']['priority']['name']
            self.dateModified = makeTheDate(task_info['fields']['dateModified'])
            self.dateCreated = makeTheDate(task_info['fields']['dateCreated'])
            self.author = task_info['fields']['authorPHID']
            self.assignee = task_info['fields']['ownerPHID']
            self.status = task_info['fields']['status']['name']
            self.projects = task_info['attachments']['projects']['projectPHIDs']
            self.uri = f"{url}/T{task_info['id']}"
            self.cache = cache
            self.PhabClientLCache = PhabClientLCache
            self.transactions = transactions
        except KeyError:
            return None

    @classmethod
    def getTaskfromTaskNumber(cls, task_number, PhabClientLCache,
                              cache=None, refresh=False, lookups=False):
        '''
        Create a Task instance from a task number. First check the cache, unless refresh is True.
        If the task is not in the cache, get the task_phid from the cache or the API, and then fetch
        the task details from the API, and save to cache.'''
        task_phid = PhabClientLCache._lookupPhid(task_number, cache=cache, phid_type='tasks')
        task_info = PhabClientLCache._taskDetails(task_phid, cache=cache, refresh=refresh)
        task_instance = cls(task_info, PhabClientLCache, cache=cache)
        if lookups:
            task_instance.getAuthor()
            task_instance.getAssignee()
            task_instance.getProjects()
        return task_instance
        #     try:
        #         task_phid = cache['tasks'].dget('index', task_number)
        #         task_info = cache['tasks'].dget('data', task_phid)
        #         task_instance = cls(task_info, PhabClientLCache, cache=cache)
        #         if lookups:
        #             task_instance.getAuthor()
        #             task_instance.getAssignee()
        #             task_instance.getProjects()
        #         return task_instance
        #     except KeyError:
        #         print(f"Cache miss for {task_number} in tasks cache")
        #         pass
        # task_phid = PhabClientLCache._lookupPhid(task_number, cache=cache, phid_type='tasks')
        # task_info = PhabClientLCache._taskDetails(task_phid, cache=cache, refresh=refresh)
        # task_instance = cls(task_info, PhabClientLCache, cache=cache)
        # if lookups:
        #     task_instance.getAuthor()
        #     task_instance.getAssignee()
        #     task_instance.getProjects()
        # task_instance.saveToDB()
        # return task_instance

    @classmethod
    def getTaskPhidFromTaskNumber(cls, task_number, PhabClientLCache, cache=None, refresh=False):
        task_phid = PhabClientLCache._lookupPhid(task_number, cache=cache, phid_type='tasks')
        task_info = {'phid': task_phid}
        task_instance = cls(task_info, PhabClientLCache, cache=cache)
        return task_instance

    def refresh(self):
        self.getTaskfromTaskNumber(self.number, self.PhabClientLCache, self.caches, refresh=True)
        if self.cache:
            self.cache['tasks'].dump()

    def getAuthor(self):
        '''
        Get the Author of the task. User PHIDs are stored as a list of phids
        in phids.db. We convert the phid to Assignee's names in the Task object, however
        but we do not cache that info anywhere.
        '''
        # if isinstance(self.author, dict):
        #     return self.author
        # self.author = {'phid': self.author,
        #                'name': f"{author_data[self.author][0]} ({author_data[self.author][1]})"
        #                }
        try:
            if self.author.startswith('PHID-USER'):
                author_data = self.PhabClientLCache._getUsers([self.author], cache=self.cache)
                self.author = f"{author_data[self.author][0]} ({author_data[self.author][1]})"
        except AttributeError:
            self.author = 'No one ()'
        return self.author

    def getAssignee(self):
        '''
        Get the Assignee of the task. User PHIDs are stored as a list of phids
        in phids.db. We convert the phid to Assignee's names in the Task object, however
        but we do not cache that info anywhere.
        '''
        try:
            if self.assignee.startswith('PHID-USER'):
                assignee_data = self.PhabClientLCache._getUsers([self.assignee], cache=self.cache)
                self.assignee = f"{assignee_data[self.assignee][0]} ({assignee_data[self.assignee][1]})"
        except AttributeError:
            self.assignee = 'No one ()'
        return self.assignee

    def getStatus(self):
        return self.status

    def getPriority(self):
        return self.priority

    def getProjects(self):
        '''
        Get the projects associated with the task. The projects are stored as a list of phids
        in phids.db. We convert the phids to project names in the Task object, however
        but we do not cache that info anywhere.
        '''
        if isinstance(self.projects, dict):
            return self.projects
        projects = self.PhabClientLCache._getProjects(self.projects, cache=self.cache)
        self.projects = {i[0]: i[1] for i in projects}
        return self.projects

    @classmethod
    def getTaskTransactionsFromTaskNumber(cls, task_number, PhabClientLCache,
                                          cache=None, refresh=False, lookups=False):
        '''
        Get the transactions of the task. The transactions are stored as a list of dictionaries
        in phids.db. We convert the dictionaries to human readable strings in the Task object, however
        but we do not cache that info anywhere.
        '''
        task_phid = PhabClientLCache._lookupPhid(task_number, cache=cache, phid_type='tasks')
        task_info = {'phid': task_phid}
        task_instance = cls(task_info, PhabClientLCache, cache=cache)
        transactions_raw = PhabClientLCache.getTransactions(task_instance.task_phid)
        transactions_raw = cls.botCleanup(transactions_raw)
        transactions_raw.sort(key=lambda x: x['dateCreated'])
        # save to db
        transactions = []
        for transaction_item in transactions_raw:
            transactions.append(Transaction(task_phid, transaction_item, PhabClientLCache, cache=cache))
        return transactions

    @staticmethod
    def botCleanup(transactions):
        '''
        Clean up the task. This is a bot operation, and we do not cache the results.
        '''
        bots = ['PHID-USER-idceizaw6elwiwm5xshb', 'PHID-USER-gzxwjo5i7iwtojwoibwt', 'PHID-USER-j4uyesgqhubl2dywl4xd']
        human_comments_only = list(filter(lambda case:
                                   case['type'] == 'comment' and case['authorPHID'] not in bots, transactions))
        return human_comments_only

    def saveToDB(self):
        '''
        The tasks.db has auto_dump disabled, thus we save to disk on demand.
        '''
        if self.cache:
            self.cache['tasks'].dadd('index', (self.number, self.task_phid))
            self.cache['tasks'].dump()
        return True
