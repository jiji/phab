#!/usr/bin/env python

"""
SPDX-License-Identifier: GPL-3.0-or-later

Phabricator command line client. This builds upon the library from
phabricator-maintenance-bot.
"""

import argparse
import configparser
import os
import sys
import logging
import re
import json
import time
from json import JSONDecodeError
from rich.console import Console
from rich.markdown import Markdown
from datetime import datetime
from pathlib import Path
from pickledb import PickleDB


from termcolor import colored

logger = logging.getLogger(__name__)

# from phab_task import Task
# from phab_client_lcache import PhabClientLCache
try:
    from phab_task import Task
    from phab_client_lcache import PhabClientLCache
except ImportError:
    from .phab_task import Task
    from .phab_client_lcache import PhabClientLCache


def timeIt(func):
    def wrapit(*args, **kwargs):
        start_time = time.perf_counter()
        result = func(*args, **kwargs)
        end_time = time.perf_counter()
        logger.debug("%s executed in %s ms",
                     func.__name__, (end_time - start_time)*1000)
        return result
    return wrapit


def makeTheDate(date):
    _date = {'timestamp': int(date),
             'human': f"{datetime.fromtimestamp(int(date)).strftime('%Y-%m-%d %H:%M')} UTC"
             }
    return _date


def normaliseLength(string_, length=72):
    if len(string_) >= length:
        return string_[:length]
    else:
        return string_.ljust(length, ' ')


def demojify(text):
    """
    Replaces text that contains emoji with nothing.
    """
    r_pattern = re.compile(pattern='['
                                   u'\U0001F600-\U0001F64F'  # emoticons
                                   u'\U0001F300-\U0001F5FF'  # symbols & pictographs
                                   u'\U0001F680-\U0001F6FF'  # transport & map symbols
                                   u'\U0001F1E0-\U0001F1FF'  # flags (iOS)
                                   u'\U00002700-\U000027BF'  # Dingbats
                                   u'\U0001F9F9'  # broom
                                   u'\U0001F9D8'  # yoga
                                   u'\U0001F94C'  # curling
                                   u'\U00002640'  # sign
                                   u'\U0000200D'  # Zero with joiner
                                   u'\U0001F94B'  # karate
                                   '⎈'  # helm
                                   ']+', flags=re.UNICODE)
    return r_pattern.sub(r'', text)


def getConfig(config_file=None, site=None):
    """
    Parse a configuration file and load its values.
    """
    config_name = Path("phab.cfg")
    default_paths = [
        Path(Path.cwd() / config_name),
        Path(os.environ.get("XDG_CONFIG_HOME", Path.home() / ".config" / "phab" / config_name)),
        Path(Path.home() / ".phab" / config_name),
        Path("/etc/phab" / config_name),
    ]

    config = configparser.ConfigParser()
    """
    TODO: Check file permissions since API keys will be in there
    """
    if config_file:
        config.read(config_file)
    else:
        try:
            for candidate in default_paths:
                if isinstance(candidate, Path) and candidate.is_file():
                    config.read(candidate)
                    continue

        except FileNotFoundError:
            print("Config file not found. Looked in: %s", default_paths)
            raise
    site_ = ''
    if site:
        site_ = site
    else:
        site_ = config['main']['default']
    key = config[site_]['key']
    default_project = config[site_]['default_project']
    url = config[site_]['url']
    username = config[site_]['username']

    if sys.platform == 'linux':
        default_cache_path = Path(
            os.environ.get("XDG_CACHE_HOME",
                           Path.home() / ".cache/phab" / Path(default_project))
        )
    else:
        default_cache_path = Path(Path.home() / ".phab/cache" /
                                  Path(default_project))
    cache_path = config[site_].get('cache_path', default_cache_path)

    return {'key': key, 'default_project': default_project, 'url': url, 'username': username, 'cache_path': cache_path}


@timeIt
def init_db(cache_path):
    '''
    If cache_path is defined, load the cache files, or create them if they don't exist.
    We have 2 cache files:
    * cache['phids'] for phids.db: This is where we store the PHIDs of users, projects, tags, and columns
    * cache['tasks'] for tasks.db: This is where we store the task details, and the index of task numbers to PHIDs

    We save the phids.db, but not the tasks.db, since we may be making multiple changes to the tasks.db
    '''
    if cache_path is None:
        return None
    # mkdir doesn't parse ~ so expand the path first
    full_path = Path(cache_path).expanduser()
    full_path.mkdir(parents=True, exist_ok=True)
    try:
        phids = PickleDB(Path(cache_path, 'phids.db'))
        for key in ['usernames', 'projects', 'tags', 'columns', 'phids']:
            if key not in phids.all():
                phids[key] = {}
        phids.save()

        tasks = PickleDB(Path(cache_path, 'tasks.db'))
        for key in ['data', 'index']:
            if key not in tasks.all():
                tasks[key] = {}

    except JSONDecodeError:
        print("Cache files are corrupted. Please delete them and try again.")
        print(f"Cache files are under: {cache_path}")
        sys.exit(0)
    return {'phids': phids, 'tasks': tasks}


def checkProjectPhidIsOne(project_phids, search_term):
    '''
    When we are searching for a tag, the API may return
    multiple tags that match the search term, or none at all.
    If we find only one tag, we return True, and we can continue
    any operation we were doing. However, if we find multiple tags,
    we can't proceed to any actions, and we need to the user to choose
    the PHID of the tag they want to use.

    TODO: Maybe we shouldn't exit here, but return the list of tags, their PHIDs
    and await user input to proceed.
    '''
    if len(project_phids) == 0:
        print(f"We can't find a tag named {search_term}")
        sys.exit(1)
    if len(project_phids) == 1:
        return True
    else:
        print(f"Multiple tags match \'{search_term}\', please specify by directly using the tag's PHID")
        for name, phid in project_phids.items():
            print(colored(phid, 'white', attrs=['bold']), colored(name, 'yellow'))
        sys.exit(1)


def mapColumns(client, project_phid):
    """
    Map column PHIDs to human names
    TODO: make case insensitive
    """
    columns = client.getColumns(project_phid)
    mapping = {}
    for i in columns['data']:
        if i['fields']['isHidden'] is not True:
            mapping[demojify(i['fields']['name']).replace(" ", "")] = i['phid']
    return mapping


def _mapColumns(client, cache, project_phid, phid_type='columns'):
    if cache:
        with cache['phids'] as c:
            mapping = {}
            # create an empty dict in the columns key
            if len(c.get('columns')) != 0:
                return cache['phids']['columns']
            else:
                logger.debug("Cache miss for columns of %s", project_phid)
                mapping = mapColumns(client, project_phid)
                c[phid_type] = mapping
                return mapping
    else:
        return mapColumns(client, project_phid)


try:
    terminalWidth = os.get_terminal_size()[0]
except OSError:
    terminalWidth = 80


@timeIt
def viewTask(task_number, client, cache, console=Console()):
    '''
    TODO: Move to Task class
    '''
    task = Task.getTaskfromTaskNumber(task_number, client,
                                      cache=cache, refresh=False, lookups=True)
    print("")
    # print(colored(f"{task.number} {task.title} {task.uri}", 'yellow'))
    print(f"{colored(task.number, 'yellow', attrs=['bold'])} ",
          f"{colored(task.title, 'white', attrs=['bold'])}"
          f" {colored(task.uri, 'blue', attrs=['bold'])}")
    print("")
    print(f"{colored('Created: ', 'magenta')}{task.dateCreated['human']}:",
          f"{colored('Last Modified: ', 'magenta')}{task.dateModified['human']}")
    print(colored(f'{"-"*terminalWidth}', 'yellow', attrs=['bold']))
    console.print(Markdown(task.description))
    # print(task.description)
    print(colored(f'{"-"*terminalWidth}', 'yellow', attrs=['bold']))
    print(f"{colored('Author: ', 'blue', attrs=['bold'])}{task.author},"
          f" {colored('Assignee: ', 'blue', attrs=['bold'])}{task.assignee}"
          )
    print(f"{colored('Status: ', 'magenta', attrs=['bold'])}{task.status},"
          f" {colored('Priority: ', 'magenta', attrs=['bold'])}{task.priority}")
    print(f"{colored('Projects: ', 'green', attrs=['bold'])}{', '.join(str(i[1]) for i in task.getProjects().items())}")
    print('\n')


@timeIt
def getComments(task_number, client, cache, limit, console=Console()):
    '''
    gerritbot's PHID is PHID-USER-idceizaw6elwiwm5xshb
    stashbot is  PHID is 'PHID-USER-j4uyesgqhubl2dywl4xd
    ops-monitoring-bot's PHID is PHID-USER-gzxwjo5i7iwtojwoibwt
    TODO: Create a class to handle transactions
    '''
    console = Console()
    task_number = task_number
    transactions = Task.getTaskTransactionsFromTaskNumber(task_number, client, cache=cache)
    for transaction in transactions[-limit:]:
        console.print()
        console.rule(f"[bold white]On [magenta]{transaction.dateCreated['human']}," +
                     f" [bold white]{transaction.getAuthor()} [/bold white] wrote:",
                     align='left', style='bright_yellow')
        console.print(Markdown(transaction.transaction_content))
        console.rule('', style='white')
    console.print()


def moveColumn(task_number, to_column, project_columns, client, cache):
    '''We assume that when we get here, we have either cached the
       columns or we have just fetched them'''
    to_column = to_column.replace(" ", "")
    task = Task.getTaskfromTaskNumber(task_number, client,
                                      cache=cache, refresh=False)
    to_column_phid = [project_columns[to_column]]
    client.moveColumns(task.task_phid, to_column_phid)
    task = Task.getTaskfromTaskNumber(task_number, client, cache=cache, refresh=True)
    logger.info("Moved %s to %s", task.number, to_column)


def rmTag(task_number, tag, client, cache):
    '''
    We can directly use the PHID of the tag we want to remove
    If not, we will do a search for that Tag (by name). If more than one tags match the tag pattern,
    we print the list of matching tags and their PHIDs, and let the user chose.
    If the task never had the tag, the request to remove it will be made, even if it is a noop

    TODO: Move to Task class
    '''
    task = Task.getTaskfromTaskNumber(task_number, client,
                                      cache=cache)
    tag_phid = ''
    if tag.startswith('PHID-PROJ'):
        tag_phid = tag
    else:
        tag_phid_info = client.lookupProjectPhid(tag)
        if checkProjectPhidIsOne(tag_phid_info, tag):
            tag_phid = tag_phid_info[tag]
    client.removeProjectByPhid(tag_phid, task.task_phid)

    task = Task.getTaskfromTaskNumber(task_number, client,
                                      cache=cache, refresh=True)
    logger.info("Removed %s from %s", tag, task.number)
    projects = client._getProjects(task.projects, cache, phid_type='projects')
    print(f"{colored('{0} Tags:'.format(task_number), 'green', attrs=['bold'])}"
          f"{', '.join(str(i[1]) for i in projects)}")


def setPriority(task_number, priority, client, cache):
    '''
    TODO: Move to Task class
    '''
    priorities = {
        'u': 'unbreak',
        't': 'triage',
        'h': 'high',
        'n': 'normal',
        'l': 'low',
        'll': 'lowest'
    }
    priorities_long = priorities.values()
    task = Task.getTaskPhidFromTaskNumber(task_number, client, cache=cache)
    if priority.lower() not in priorities and priority.lower() not in priorities_long:
        print(f"Priorities should be one of {', '.join(str(i) for i in priorities.values())}")
        sys.exit(1)
    if priority.lower() in priorities:
        priority = priorities[priority.lower()]
    client.changeTaskPriority(task.task_phid, priority)
    task = Task.getTaskfromTaskNumber(task_number, client,
                                      cache=cache, refresh=True)
    print(f"{colored(task.number, 'white', attrs=['bold'])} "
          f"({colored(task.title, 'white', attrs=['bold'])}):"
          f" Priority set to {colored(task.priority, 'yellow', attrs=['bold'])}")


def setStatus(task_number, status, client, cache):
    '''
    TODO: Move to Task class
    '''
    statuses = {
        'o': 'open',
        'r': 'resolved',
        'p': 'progress',
        's': 'stalled',
        'd': 'declined',
        'dup': 'duplicate',
        'i': 'invalid'
    }
    statuses_long = statuses.values()
    if status.lower() not in statuses and status.lower() not in statuses_long:
        print(f"Statuses should be one of {', '.join(str(i) for i in statuses)}")
        sys.exit(1)
    if status.lower() in statuses:
        status = statuses[status.lower()]
    task = Task.getTaskPhidFromTaskNumber(task_number, client, cache=cache)
    client.changeTaskStatus(task.task_phid, status)
    task = Task.getTaskfromTaskNumber(task_number, client,
                                      cache=cache, refresh=True)
    print(f"{colored(task.number, 'white', attrs=['bold'])} "
          f"({colored(task.title, 'white', attrs=['bold'])}):"
          f" Status set to {colored(task.status, 'yellow', attrs=['bold'])}")


@timeIt
def main():
    # TODO: KeyboardInterrupt
    # TODO: Refactor to avoid unnecessary requests (eg getting the column list)
    config_ = getConfig()
    parser = argparse.ArgumentParser()
    # TODO: override with --project,-p flag
    # TODO: override site with --instance, -i flag
    # TODO: override config with --config, -c flag
    # TODO: skip the cache with --no-cache
    # TODO: clear the cache with --clear-cache
    # TODO: Add --order, -o, order by
    # TODO: limit results
    # TODO: Define statuses --status, -s

    parser.add_argument('command', nargs='*', type=str, help='Action to execute: ls, mv, rmtag, setprio, setstatus,' +
                        'view, comments')
    parser.add_argument('--limit', type=int, default=500, help='Limit the number of results')
    parser.add_argument("-vv", "--debug", action="store_true", help="Show debug output")
    parser.add_argument("-v", "--verbose", action="store_true", help="Show verbose output")
    args = parser.parse_args()
    if args.debug:
        logging.basicConfig(level=logging.DEBUG)
    elif args.verbose:
        logging.basicConfig(level=logging.INFO)

    client = PhabClientLCache(config_['url'], config_['username'], config_['key'])
    cache = init_db(config_['cache_path'])
    project_phid = client._lookupPhid(f"#{config_['default_project']}", cache, 'projects')
    project_columns = _mapColumns(client, cache, project_phid)
    column_phid = []
    if 'ls' in args.command:
        try:
            if args.command[1]:
                column_ = args.command[1].replace(" ", "")
                column_phid = [project_columns[column_]]
                tasks_in_column = client.getTasksWithProject(project_phid,
                                                             statuses=['open', 'stalled', 'progress'],
                                                             column_phids=column_phid, return_task_details=True,
                                                             cache=cache, limit=args.limit)
                for task in tasks_in_column:
                    print(
                        colored(normaliseLength(task['priority'], 12), 'yellow'),
                        colored(task['id'], 'white', 'on_grey'),
                        normaliseLength(task['title']),
                        colored('Updated: ', 'magenta') +
                        f"{makeTheDate(task['dateModified'])['human']}"
                    )
        # If not arguments are given to 'ls', just list the available columns
        except IndexError:
            print('')
            for name, column_phid in project_columns.items():
                print(f"{colored(normaliseLength(name, 30), 'yellow')}")
            print('')
        except KeyError:
            print(f'Column {args.command[1]} does not exist')
        except KeyboardInterrupt:
            sys.exit(1)

    if 'mv' in args.command:
        moveColumn(args.command[1], args.command[2], project_columns, client, cache)

    if 'rmtag' in args.command:
        rmTag(args.command[1], args.command[2], client, cache)

    if 'setprio' in args.command or 'sp' in args.command:
        setPriority(args.command[1], args.command[2], client, cache)

    if 'setstatus' in args.command or 'st' in args.command:
        setStatus(args.command[1], args.command[2], client, cache)

    if 'view' in args.command or 'v' in args.command:
        viewTask(args.command[1], client, cache)

    if 'comments' in args.command or 'c' in args.command:
        getComments(args.command[1], client, cache, args.limit)

    if 'read' in args.command or 'r' in args.command:
        viewTask(args.command[1], client, cache)
        getComments(args.command[1], client, cache, args.limit)


if __name__ == '__main__':
    sys.exit(main())
