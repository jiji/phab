#!/usr/bin/env python

"""
SPDX-License-Identifier: GPL-3.0-or-later

TODO: Add lastModified in index
"""
from datetime import datetime
import logging
import time

logger = logging.getLogger(__name__)

def makeTheDate(date):
    _date = {'timestamp': int(date),
             'human': f"{datetime.fromtimestamp(int(date)).strftime('%Y-%m-%d %H:%M')} UTC"
             }
    return _date


def timeIt(func):
    def wrapit(*args, **kwargs):
        start_time = time.perf_counter()
        result = func(*args, **kwargs)
        end_time = time.perf_counter()
        logger.debug("%s executed in %s ms",
                     func.__name__, (end_time - start_time)*1000)
        return result
    return wrapit


class Transaction():
    @timeIt
    def __init__(self, task_phid, transaction_data, PhabClientLCache, url='https://phabricator.wikimedia.org',
                 cache=None, refresh=False):
        self.trans_id = transaction_data['id']
        self.phid = transaction_data['phid']
        self.author = transaction_data['authorPHID']
        self.dateCreated = makeTheDate(transaction_data['dateCreated'])
        self.dateModified = makeTheDate(transaction_data['dateModified'])
        self.transaction_content = self.getLatestVersionOfTransaction(transaction_data['comments'])['content']['raw']
        self.cache = cache
        self.PhabClientLCache = PhabClientLCache

    def getLatestVersionOfTransaction(self, transaction_comments):
        if len(transaction_comments) == 1:
            return transaction_comments[0]
        versions = []
        for pos, version in enumerate(transaction_comments):
            if version['removed'] is True:
                continue
            versions.append((version['id'], version['version'], pos))
        latest_comment_id = max(versions, key=lambda x: x[1])
        return transaction_comments[latest_comment_id[2]]

    def getAuthor(self):
        '''
        Get the Author of the task. User PHIDs are stored as a list of phids
        in phids.db. We convert the phid to Assignee's names in the Task object, however
        but we do not cache that info anywhere.
        '''
        # if isinstance(self.author, dict):
        #     return self.author
        # self.author = {'phid': self.author,
        #                'name': f"{author_data[self.author][0]} ({author_data[self.author][1]})"
        #                }
        try:
            if self.author.startswith('PHID-USER'):
                author_data = self.PhabClientLCache._getUsers([self.author], cache=self.cache)
                self.author = f"{author_data[self.author][0]} ({author_data[self.author][1]})"
        except AttributeError:
            self.author = 'No one ()'
        return self.author

        #return transaction_comments[latest_comment_id[2]][0]
