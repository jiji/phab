#!/usr/bin/env python

"""
SPDX-License-Identifier: GPL-3.0-or-later

Phabricator command line client. This builds upon the library from
phabricator-maintenance-bot.
"""

import time
import logging
from phabricator_maintenance_bot.lib import Client

logger = logging.getLogger(__name__)

def timeIt(func):
    def wrapit(*args, **kwargs):
        start_time = time.perf_counter()
        result = func(*args, **kwargs)
        end_time = time.perf_counter()
        logger.debug("%s executed in %s ms",
                     func.__name__, (end_time - start_time)*1000)
        return result
    return wrapit


class PhabClientLCache(Client):
    pagination_limit = 100

    def getTasksWithProject(self, project_phid, continue_=None, statuses=None, column_phids=None,
                            return_task_details=False, cache=None, refresh=False, limit=500, results_count=0):
        if limit <= self.pagination_limit:
            self.pagination_limit = limit
        r = self._getTasksWithProjectContinue(project_phid, continue_, statuses=statuses,
                                              column_phids=column_phids)
        cursor = r['cursor']
        tasks_only = list(filter(lambda case:
                          case['type'] == 'TASK', r['data']))
        remaining_results = limit - results_count
        r['data'] = tasks_only[:remaining_results]
        del tasks_only
        # TODO: we could check if the cache is up to date
        #       by checking the last modified date of the task
        # TODO: We could add an option to refresh the cache
        # For now, this updates the tasks cache every time we ls
        # TODO: add when the tasks db was last updated
        # This code is a bit of a mess, but it works
        if cache:
            with cache['tasks'] as c:
                for i in r['data']:
                    phid = i['phid']
                    id = f"T{i['id']}"
                    c['data'][phid] = i
                    c['index'][id] = phid
                c.all()
        for case in r['data']:
            if return_task_details:
                task_details = {
                    'id': f"T{case['id']}",
                    'title': case['fields']['name'],
                    'priority': case['fields']['priority']['name'],
                    'dateModified': case['fields']['dateModified'],
                }
                yield task_details
            else:
                yield case['phid']
            results_count += 1
        if cursor.get('after') and results_count < limit:
            for case in self.getTasksWithProject(project_phid, cursor['after'], statuses=statuses,
                                                 column_phids=column_phids,
                                                 return_task_details=return_task_details,
                                                 cache=cache, refresh=False, limit=limit, results_count=results_count):
                yield case

    def _getTasksWithProjectContinue(self, project_phid, continue_=None, statuses=None, column_phids=[]):
        params = {
            'limit': self.pagination_limit,
            'constraints': {
                'projects': [project_phid],
                'columnPHIDs': column_phids
            },
            "order": "updated",
            'attachments': {
                "projects": True,
            }
        }
        if continue_:
            params['after'] = continue_
        if statuses:
            params['constraints']['statuses'] = statuses
        return self.post('maniphest.search', params)

    def moveColumns(self, task_phid, to_column):
        self.post('maniphest.edit', {
            'objectIdentifier': task_phid,
            'transactions': [{
                'type': 'column',
                'value': to_column,
            }]
        })

    def getUsers(self, user_phids=[]):
        userdata = {}
        if '0' in user_phids or None in user_phids:
            userdata.update({'0': ('No one', '')})
            try:
                user_phids.remove('0')
            except ValueError:
                user_phids.remove(None)
        if len(user_phids) == 0:
            return userdata
        r = self.post('user.search', {'constraints': {
            'phids': user_phids}})
        userdata.update({i['phid']: (i['fields']['username'], i['fields']['realName']) for i in r['data']})
        return userdata

    def _getUsers(self, user_phids=[], cache=None, phid_type='usernames'):
        if not cache:
            return self.getUsers(user_phids)
        else:
            userdata = {}
            for user_phid in user_phids:
                try:
                    userdata.update({user_phid: tuple(cache['phids'][phid_type][user_phid])})
                except KeyError:
                    logger.debug("Cache miss for %s", user_phid)
                    userdata_ = self.getUsers([user_phid])
                    for user_phid, (username, realName) in userdata_.items():
                        if not cache['phids'][phid_type]:
                            cache['phids'][phid_type] = {}
                        cache['phids'][phid_type][user_phid] = (username, realName)
                        userdata.update({user_phid: (username, realName)})
                    cache['phids'].save()
            return userdata

    def getProjects(self, project_phids=[]):
        r = self.post('project.search', {'constraints': {
            'phids': project_phids}})
        return [(i['phid'], i['fields']['name']) for i in r['data']]

    @timeIt
    def _getProjects(self, project_phids=[], cache=None, phid_type='projects'):
        # even if we are missing one project, we will lookup all projects since
        # it is one call to the API
        #
        # The cache['phids']['projects'] may need some work, as we are storing
        # our "home" project as "#serviceops": "PHID-PROJ-ffms22arq2fzb25pcbwn"
        # and all other project tags as "PHID-PROJ-ffms22arq2fzb25pcbwn": "serviceops"
        # But this is not a perfect world, is it?

        if not cache:
            return self.getProjects(project_phids)
        else:
            projectdata = []
            try:
                for project_phid in project_phids:
                    projectdata.append((project_phid, cache['phids'][phid_type][project_phid]))
                return projectdata
            except KeyError:
                logger.debug("Cache miss for %s", project_phid)
                projects = self.getProjects(project_phids)
                for project_phid, project in projects:
                    cache['phids'][phid_type][project_phid] = project
                cache['phids'].save()
                return projects
            except TypeError:
                pass

    def lookupProjectPhid(self, query):
        r = self.post('project.search', {'constraints': {
            'query': query}})
        return {i['fields']['name']: i['phid'] for i in r['data']}

    def changeTaskPriority(self, task_phid, priority):
        self.post('maniphest.edit', {
            'objectIdentifier': task_phid,
            'transactions': [{
                'type': 'priority',
                'value': priority,
            }]
        })

    def changeTaskStatus(self, task_phid, status):
        self.post('maniphest.edit', {
            'objectIdentifier': task_phid,
            'transactions': [{
                'type': 'status',
                'value': status,
            }]
        })

    @timeIt
    def _lookupPhid(self, label, cache=None, phid_type=None):
        if not cache:
            return self.lookupPhid(label)
        phid_type_ = phid_type or 'phids'
        cache_db = 'tasks' if phid_type_ == 'tasks' else 'phids'
        cache_db_table = 'index' if phid_type_ == 'tasks' else phid_type_
        with cache[cache_db] as c:
            try:
                return c[cache_db_table][label]
            except KeyError:
                logger.debug("Cache miss for %s in %s", label, phid_type)
                phid = self.lookupPhid(label)
                c[cache_db_table][label] = phid
                return phid

    def taskDetails(self, phid):
        """Lookup details of a Maniphest task."""
        params = {
            'constraints': {
                'phids': [phid],
            },
            'attachments': {
                "projects": True,
            }
        }
        r = self.post('maniphest.search', params)
        if phid in r['data'][0]['phid']:
            return r['data'][0]
        raise Exception('No task found for phid %s' % phid)

    def _taskDetails(self, phid, cache=None, refresh=False):
        """Lookup details of a Maniphest task."""
        if not cache:
            return self.taskDetails(self, phid)
        else:
            if phid in cache['tasks']['data'] and not refresh:
                return cache['tasks']['data'][phid]
            else:
                if not refresh:
                    logger.debug("Cache miss for %s in tasks", phid)
                else:
                    logger.debug("Refreshing cache for %s", phid)
                r = self.taskDetails(phid)
                with cache['tasks'] as c:
                    c['data'][phid] = r
                    c.all()
                return r

    def getTransactions(self, phid):
        r = self.post('transaction.search', {'objectIdentifier': phid})
        if 'data' in r:
            return r['data']
        raise Exception('No transaction found for phid %s' % phid)
